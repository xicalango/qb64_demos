SCREEN 13, 0, 1, 2

_TITLE "Cruising in the 80s"

RANDOMIZE TIMER

horizon = _HEIGHT / 2


midx = _WIDTH / 2
midy = horizon - 50

ybase = _HEIGHT * 10
xwidth = _WIDTH * 50
xoffset = xwidth / 2

nylines = 10
nxlines = 30

COLOR 15

numtrees = 10

COLOR 8

CIRCLE (0, horizon), 0

FOR i = 1 TO numtrees - 1
    mountainHeight = INT(RND * 50)
    treeHeight = INT(RND * 10) + 5

    treeX = i * (_WIDTH / numtrees)

    COLOR 8
    LINE STEP(0, 0)-(treeX, horizon - mountainHeight)

    COLOR 6
    LINE STEP(0, 0)-STEP(0, -treeHeight)
    COLOR 10
    CIRCLE STEP(0, 0), treeHeight / 3
    PAINT STEP(0, 0)
    LINE STEP(0, 0)-STEP(0, treeHeight / 3)
    COLOR 6
    LINE STEP(0, 0)-STEP(0, (treeHeight * 2) / 3)
NEXT i


COLOR 8
LINE STEP(0, 0)-(_WIDTH, horizon)
LINE STEP(0, 0)-(0, horizon)
CIRCLE STEP(_WIDTH / 2, 0), 0
PAINT STEP(0, -1)

COLOR 14
CIRCLE (20, 15), 10
PAINT STEP(0, 0)



VIEW SCREEN(0, horizon)-(_WIDTH - 1, _HEIGHT - 1)


offset = 0

DO
    PCOPY 1, 2
    CLS
    _LIMIT 30

    COLOR 9

    FOR i = 0 TO nxlines
        posx = (i * (xwidth / nxlines)) - xoffset
        LINE (posx, ybase)-(midx, midy)
    NEXT i

    COLOR 1

    LINE (0, horizon)-(_WIDTH, horizon)

    FOR i = 0 TO nylines
        posy = ((i * ((_HEIGHT - horizon) / nylines)) + horizon) + offset
        LINE (0, posy)-(_WIDTH, posy)

    NEXT i

    offset = offset + 1
    offset = offset MOD (_HEIGHT - horizon) / nylines


LOOP UNTIL INKEY$ = CHR$(27)

