# qb64_demos

Some just for fun graphics (and maybe others in the future) demos written in basic for [QB64](https://www.qb64.org/).

# 80sdriving

Driving in the 80s == driving on a grid

![driving](screenshots/driving.png)

# polka

Color changing polka dots!

![polka dots](screenshots/polka.png)