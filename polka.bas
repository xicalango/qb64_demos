CLS

SCREEN 12

CONST CIRCLE_SIZE = 10
CONST CELL_SIZE = CIRCLE_SIZE * 4

CONST CIRCLE_COLOR = 10
CONST CIRCLE_COLOR_2 = 11
CONST BACKGROUND_COLOR = 1

num_circles_x = _WIDTH / CELL_SIZE
num_circles_y = _HEIGHT / CELL_SIZE

_TITLE "POLKA"

PAINT (0, 0), BACKGROUND_COLOR

FOR j = 0 TO num_circles_y
    pos_y = j * CELL_SIZE
    FOR i = 0 TO num_circles_x
        pos_x = i * CELL_SIZE + ((j MOD 2) * CIRCLE_SIZE * 2)
        IF (j + i) MOD 2 = 0 THEN
            cc = CIRCLE_COLOR
        ELSE
            cc = CIRCLE_COLOR_2
        END IF
        CIRCLE (pos_x, pos_y), CIRCLE_SIZE, cc
        PAINT STEP(0, 0), cc
        PAINT STEP(-1, -1), cc
    NEXT i
NEXT j

phi = 0
DO
    i = INT(SIN(degtorad(phi)) * 127) + 127
    j = INT(COS(degtorad(phi)) * 127) + 127
    phi = (phi + 1) MOD 360
    _DELAY .001
    _PALETTECOLOR CIRCLE_COLOR, _RGB32(i, 0, 255)
    _PALETTECOLOR CIRCLE_COLOR_2, _RGB32(0, j, 0)
    _PALETTECOLOR BACKGROUND_COLOR, _RGB32(255 - i, 32, j)
    IF INKEY$ = CHR$(27) THEN EXIT DO
LOOP


FUNCTION degtorad (deg)
    degtorad = deg * _PI / 180
END FUNCTION








