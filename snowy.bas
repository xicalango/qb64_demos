SCREEN 12, 0, 1, 2

_TITLE "Snowy"

RANDOMIZE TIMER

TYPE Snowflake
    start_x AS INTEGER
    x AS INTEGER
    y AS SINGLE

    dy AS SINGLE

    freq AS INTEGER
    amp AS INTEGER

    c AS INTEGER

    size AS INTEGER
END TYPE

render_target& = _DEST
snow_map& = _NEWIMAGE(_WIDTH, _HEIGHT, 12)

_SOURCE snow_map&

CONST nflakes = 1000

DIM flakes(nflakes) AS Snowflake

FOR i = 1 TO nflakes
    init_flake flakes(i)
NEXT i

_PALETTECOLOR 1, _RGB32(200, 244, 255)
_PALETTECOLOR 2, _RGB32(255, 255, 255)
_PALETTECOLOR 3, _RGB32(161, 211, 255)
_PALETTECOLOR 4, _RGB32(222, 238, 255)
_PALETTECOLOR 5, _RGB32(0, 122, 0)

_DEST snow_map&
COLOR 2

FOR i = 1 TO 20
    x = INT(RND * _WIDTH)
    y = INT(RND * _HEIGHT)
    size = INT(RND * 10) + 10
    CIRCLE (x, y), size
    PAINT STEP(0, 0)
NEXT i

_DEST renger_target&


DO
    PCOPY 1, 2
    CLS
    _LIMIT 30

    _DEST snow_map&
    FOR i = 1 TO nflakes
        update flakes(i)
    NEXT i

    _DEST render_target&

    _PUTIMAGE

    FOR i = 1 TO nflakes
        render flakes(i)
    NEXT i



LOOP UNTIL INKEY$ = CHR$(27)

SUB init_flake (flake AS Snowflake)
    flake.y = 0
    flake.x = INT(RND * _WIDTH)
    flake.start_x = flake.x
    flake.dy = RND * 2 + 1
    flake.freq = INT(RND * 5)
    flake.amp = INT(RND * 20)
    flake.size = INT(RND * 2)
    flake.c = INT(RND * 4) + 1
END SUB


SUB update (flake AS Snowflake)

    IF INT(flake.y) >= _HEIGHT OR ((flake.x > 0 AND flake.x < _WIDTH) AND POINT(flake.x, flake.y)) <> 0 THEN
        COLOR flake.c
        CIRCLE (flake.x, flake.y), 1

        init_flake flake
    ELSE
        flake.y = flake.y + flake.dy
        flake.x = get_computed_x(flake)
    END IF
END SUB

SUB render (flake AS Snowflake)
    COLOR flake.c
    CIRCLE (flake.x, flake.y), flake.size
END SUB

FUNCTION get_computed_x (flake AS Snowflake)
    progression = get_progression(flake)
    delta = flake.amp * SIN((2 * _PI * flake.freq) * progression)
    get_computed_x = flake.start_x + delta
END FUNCTION


FUNCTION get_progression (flake AS Snowflake)
    get_progression = flake.y / _HEIGHT
END FUNCTION

